Memory Manager Simulator
===================================

This is a simple application that simulates the concept of memory management by Operating System Kernel.

This is a short user guide on getting started to use it or extend it.

Intro
.................

1.0 Running Application

	Check out source code (if you already do not have) from bitbucket at https://bitbucket.org/kenny_kosgei/memsim
	to a local repo and open a Netbeans ID to open the Netbeans project checked out.

	Click 'Run' from NetBeans

2.0 User Interface

	Screen is divided into 3 main sections
	1. Strategy : Where you select the Memory management criteria. Best Fit is selected by default
	2. Memory Graph Settings : Here is where you specify whether the memory graph should draw out the free blocks availa
	   ble for allocation to processes or not
	   You can also defragment the partitions by re-initializing/reformating memory into 16MB blocks, while retaining running processes in memory

	3. Main content:
		Process table : shows variaous attributes of the running processes. You can create more processes by selecting
		"Create Process..". New Processes are instantly added to the process table.

		Processes can be closed by using "Kill Process" button after selecting the processes in the the process table

		Memory Graph: Is a Java2D drawn area that is drawn according to the realtime status of the Memory/RAM.
		Occupied Blocks are grey while Free Blocks are marked by green-bordered white blocks.

	4. Menu : From the Menu, you can see the About Dialog as well as exit the application.

Simulation Process
....................

3.0 Strategies

	Best Fit
`	--------
	This is quite obvious. The Memory is searched from beggining to the very end to find a block (a combination of blocks) that
	leave minimum wastage of memory.

	You can observe this by paying attention to the memory graph after creating a process. Normally at the start processes are more likely
	to be allocated blocks towards high addresses for Best Fit strategy.
	This translates to processes in the Memory graph appearing at the end (most of the times)

	First Fit
	------------
	The memory manager allocates the earliest block (combination of blocks) that it encounters while searching for fitting blocks to
	fulfil a memory request. This means that you expect most of the time processes to be assigned lower address ranged blocks.

	In the memory graph you'll see the processes being allocated from the begining (most of the time).


	Worst Fit
	-------------
	To simulate worst fit; create a process say of 140MB and then kill it. If this is the largest block created after killing the process,
	note the address ranges of the killed process (before you kill it). create another one of say 8MB. The 140MB blick should be assigned to this new process of
	8MB.


Application Design
......................

4.0 Assumptions

	2GB (2048MB) Memory Module/RAM
	1 memory cell equivalent to 1MB
	Standard Memory sectors are made up of 16 cells (16MB)
	Processes do not grow in Memory i.e once a process is allocated a specified space, it can only use that space till it exits/gets killed

	Important Components
	--------------------
	Memory Manager : manages all memory operations
	Process Manager : creates and destroys processes
	Memory Graph: Visualizes memory partitions

NB://Check source code comments and JavaDoc for info of internal workings

Kenny on GTN-453
