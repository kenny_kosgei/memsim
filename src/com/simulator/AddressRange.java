package com.simulator;

/**
 * Used to hold a memory range allocated to process
 *
 * @author Cathering S.
 */
public class AddressRange {

    private int addressFrom;
    private int addressTo;

    public AddressRange(int addressFrom, int addressTo) {
	this.addressFrom = addressFrom;
	this.addressTo = addressTo;
    }

    public int getAddressFrom() {
	return addressFrom;
    }

    public void setAddressFrom(int addressFrom) {
	this.addressFrom = addressFrom;
    }

    public int getAddressTo() {
	return addressTo;
    }

    public void setAddressTo(int addressTo) {
	this.addressTo = addressTo;
    }
    
    /**
     * the size of the memory range referenced. 
     *
     * @return 0 if (addressTo - addressFrom) <=0, anything positive is a valid range
     */
    public int calculateSize(){
	int size = ((addressTo - addressFrom) +1);
	return size > 0 ? size : 0;
    }

    @Override
    public String toString() {
        return "size:" + calculateSize() + "MB, address_range:" + getAddressFrom() + "-" + getAddressTo(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
