package com.simulator;

/**
 *Constants that denote the various strategies in memory management/allocation
 * @author Catherine S.
 */
public enum Algorithm {

    BEST_FIT,
    FIRST_FIT,
    WORST_FIT;
  
}
