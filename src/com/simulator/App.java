package com.simulator;

import com.simulator.ui.AppHome;
import com.simulator.ui.Splash;
import java.util.TimerTask;

/**
 *
 * @author Catherine S.
 */
public class App {

    private static AppHome appHome;
    final private Splash splashScreen;
    private TimerTask timerTask;

    public App() {
        //show splash screen for a while
        appHome = new AppHome();
        splashScreen = new Splash();
        splashScreen.setLocationRelativeTo(null);

        //take some time to display the main application
        timerTask = new TimerTask() {
            @Override
            public void run() {
                appHome.setVisible(true);
                splashScreen.dispose();
            }
        };
        appHome.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new App();
            }
        });
    }

    public static AppHome getAppHome() {
	return appHome;
    }
    
    
}
