package com.simulator;

/**
 * General exception for illegal actions
 * 
 * @author Catherine S.
 */
public class IllegalOperationException extends Exception {

    /**
     * Creates a new instance of
     * <code>OutOfMemoryException</code> without detail message.
     */
    public IllegalOperationException() {
    }

    /**
     * Constructs an instance of
     * <code>OutOfMemoryException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public IllegalOperationException(String msg) {
	super(msg);
    }
}
