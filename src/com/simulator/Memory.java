package com.simulator;

/**
 * Models a memory module such as the RAM of a computer.
 * It has cells; memory cell which cumulatively make up memory
 * @author Catherine S.
 */
public class Memory {

    private final int memorySize;
    private final MemoryBlock memoryBlocks[];
    public static final int SECTOR_SIZE=16; //equivalent to 16MB

    public Memory(int memorySize) {
        this.memorySize = memorySize;
        //create equivalent number of memory blocks
	memoryBlocks = new MemoryBlock[memorySize];
	for (int i = 0; i < memoryBlocks.length; i++) {
	    memoryBlocks[i] = new MemoryBlock(0, false, i); //block inherits index for address location
	}
    }

    /**
     * A memory block represents a memory cell.
     * In this application, it represents a Megabyte block of memory
     * @return
     */
    public MemoryBlock[] getMemoryBlocks() {
        return memoryBlocks;
    }

    /**
     *Query the total memory
     * @return memory size in megabytes
     */
    public int getMemorySize() {
        return memorySize;
    }
}
