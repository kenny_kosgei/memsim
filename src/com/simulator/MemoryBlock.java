package com.simulator;

/**
 * Simulates a memory unit in a memory module A memory Block is a Megabyte of
 * memory or 1024 Kilobytes
 *
 * @author Catherine S.
 */
public class MemoryBlock {

    public static final int BLOCK_SIZE = 1;
    private int owiningProcess;
    private boolean isUsed;
    //Address location is constant/read only after initialization of memory
    private final int addressLocation;

    /**
     * Creates an instance of a memory block.
     *
     * @param owningProcess - the process associated with this memory block
     * @param isUsed - whether or not it contains process's data. But even if empty, it belongs to the process
     * @param address - the address that selects this particular block
     */
    public MemoryBlock(int owningProcess, boolean isUsed, int address) {
	this.owiningProcess = owningProcess;
	this.isUsed = isUsed;
	this.addressLocation = address;
    }

    public int getOwningProcess() {
	return owiningProcess;
    }

    public void setOwiningProcess(int owiningProcess) {
	this.owiningProcess = owiningProcess;
    }

    public boolean isIsUsed() {
	return isUsed;
    }

    public int getAddressLocation() {
	return addressLocation;
    }


    public void setIsUsed(boolean isUsed) {
	this.isUsed = isUsed;
    }
}
