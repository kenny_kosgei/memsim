package com.simulator;

/**
 * Just an exception to mimic java.lang.OutOfMemoryError for a condition where we cannot find
 * free memory locations.
 * 
 * @author Catherine S.
 */
public class OutOfMemoryException extends Exception {

    /**
     * Creates a new instance of
     * <code>OutOfMemoryException</code> without detail message.
     */
    public OutOfMemoryException() {
    }

    /**
     * Constructs an instance of
     * <code>OutOfMemoryException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public OutOfMemoryException(String msg) {
	super(msg);
    }
}
