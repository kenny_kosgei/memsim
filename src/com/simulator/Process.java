package com.simulator;

import java.awt.Color;

/**
 * An instance of this class simulates a process entity in a real machine that
 * executes in a given constant memory heap for a given life cycle time
 *
 * @author Catherine S.
 */
public class Process {

    private final int memoryHeapRequest;
    private final long lifeTime;
    private boolean isRunning = false;
    private int processID;
    private String processName;
    private Color imageColor;
    /**
     * Maximum request for memory is 128MB
     */
    public final int MAX_MEMORY_REQUEST = 128;
    /**
     * Minimum Request for memory is 16MB
     */
    public final int MIN_MEMORY_REQUEST = 16;

    public Process(int memoryHeapRequest, long lifeTime, String processName) {
	this.memoryHeapRequest = memoryHeapRequest;
	this.lifeTime = lifeTime;
        processID=0;
        this.processName = processName;
    }

    /**
     * The amount of memory requested by this process at instantiation
     *
     * @return memory in bytes
     */
    public int getMemoryHeapRequest() {
	return memoryHeapRequest;
    }


    /**
     * Sends a signal for this process to exit.
     */
    public synchronized void terminate() {
    }

    public Color getImageColor() {
	return imageColor;
    }

    public long getLifeTime() {
	return lifeTime;
    }

    public void setImageColor(Color imageColor) {
	this.imageColor = imageColor;
    }

    public boolean isIsRunning() {
	return isRunning;
    }

    public int getProcessID() {
	return processID;
    }

    public void setProcessID(int processID) {
	this.processID = processID;
    }

    public String getProcessName() {
	return processName;
    }

    public void setProcessName(String processName) {
	this.processName = processName;
    }
    
    @Override
    public String toString() {
	return processName;
    }
}
