package com.simulator.managers;

import com.simulator.AddressRange;
import com.simulator.Algorithm;
import com.simulator.IllegalOperationException;
import com.simulator.Memory;
import com.simulator.MemoryBlock;
import com.simulator.OutOfMemoryException;
import com.simulator.util.MemoryMapAddressComparator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Manages memory by maintaining a memory map of allocated & free memory as well
 * as using the available memory management schemes to assign memory
 * resources/blocks to process per request from Process manager. Initial Memory
 * size is 2048MB or 2GB
 *
 * @author Catherine S.
 */
public class MemoryManager {

    private Memory memory;
    private Map<Integer, AddressRange> assignedMemoryMap;
    private Map<Integer, AddressRange> freeMemoryMap;
    private static Algorithm selectedAlgorithm;
    /**
     * The default block range which divides the memory
     */
    public final static int sector = 16;
    /**
     * Default memory size is 2GB = 2048MB Memory Blocks * 1024 * 2
     */
    public final static int DEFAULT_MEMORY_SIZE = 2048;

    /**
     * Private constructor to guarantee that this class is not directly
     * instantiated
     */
    private MemoryManager() {
        memory = new Memory(DEFAULT_MEMORY_SIZE); //instantiate memory
        selectedAlgorithm = Algorithm.BEST_FIT; //best fit is used by default
        freeMemoryMap = new HashMap<Integer, AddressRange>();
        assignedMemoryMap = new HashMap<Integer, AddressRange>();
        formatMemory(); //create free memory map
        System.out.println("Used Memory: " + getUsedMemorySize());
        System.out.println("Free Memory: " + getFreeMemorySize());
    }

    public Memory getMemory() {
        return memory;
    }

    public void setMemory(Memory memory) {
        this.memory = memory;
    }

    public Map<Integer, AddressRange> getAssignedMemoryMap() {
        return assignedMemoryMap;
    }

    public Map<Integer, AddressRange> getFreeMemoryMap() {
        return freeMemoryMap;
    }

    public int calculateSizeOfBlocks(AddressRange blocks[]) {
        int size = 0;
        if (blocks != null && blocks.length > 0) {
            for (AddressRange addressRange : blocks) {
                if (addressRange != null) {
                    size += addressRange.calculateSize();
                }
            }
        }
        return size;
    }

    /**
     * Assumes the array of Address Keys are referencing free memory map
     * objects.
     *
     * @param rangeKeys negative 1 (-1) multiplied integer values      * of <code>addresFrom</code> field of a memory range instance
     * @return corresponding address ranges
     */
    public AddressRange[] getRangesFromFreeMemoryMap(String rangeKeys[]) {
        List<AddressRange> rangeList = new ArrayList<AddressRange>();
        if (rangeKeys != null && rangeKeys.length > 0) {
            for (String key : rangeKeys) {
                int integerKey = Integer.parseInt(key);
                if (integerKey > 0) {
                    integerKey *= -1;
                }
                if (freeMemoryMap.containsKey(integerKey)) {
                    rangeList.add(freeMemoryMap.get(integerKey));
                }
            }
        }
        return rangeList.toArray(new AddressRange[rangeList.size()]);
    }

    public synchronized List<String[]> listFreeContiguousRanges(int memorysize) {
        List<AddressRange> rangeList = new ArrayList<AddressRange>();
        rangeList.addAll(0, freeMemoryMap.values());
        Collections.sort(rangeList, new MemoryMapAddressComparator());
        StringBuilder builder = new StringBuilder();
        List<String[]> contiguousBlocks = new ArrayList<String[]>();
        int capacitySum = 0;
        AddressRange previousRange = null;
        for (AddressRange addressRange : rangeList) {
            if (addressRange != null && (addressRangesContigous(previousRange, addressRange) || previousRange == null)) {
                capacitySum += addressRange.calculateSize();
                //continue to add-up more blocks
                builder.append(addressRange.getAddressFrom()).append("#");

                if (capacitySum >= memorysize) { //if this is enough block for the current size requested
                    capacitySum = 0; //reset counter
                    String[] split = builder.toString().split("#");
                    if (split != null && split.length > 0) {
                        contiguousBlocks.add(split);
                    }
                    builder = new StringBuilder();
                }
                previousRange= addressRange;
            } else { //means we are not on a contiguous block
                //reset counter and delete previous blocks we had arracnged
                capacitySum = 0;
                builder = new StringBuilder();
                previousRange = null;
            }
        }
        return contiguousBlocks;
    }

    /**
     * Determine whether two address ranges are contigous.
     *
     * @param rangeOne first range
     * @param rangeTwo second range
     * @return true IFF rangeOne.getAddressTo-rangeTwo.getAddressfrom = 1
     */
    protected boolean addressRangesContigous(AddressRange rangeOne, AddressRange rangeTwo) {
        boolean result = false;
        if (rangeOne != null && rangeTwo != null) {
            result = (rangeOne.getAddressTo() - rangeTwo.getAddressFrom() == -1);
        }
        return result;
    }

    /**
     * Accepts a process instance and tries to find a memory block for it using
     * the current strategies e.g best fit to assign available memory
     *
     * @param targetProcess a non-null process instance.The process should also
     * not be running already; processID > 0
     * @return the choosen address block for the program/process
     */
    public synchronized AddressRange assignMemory(com.simulator.Process targetProcess) throws IllegalArgumentException,
            OutOfMemoryException,
            IllegalOperationException {
        //using selected memory. we assign memory and return selected address range
        //It afterwards updates the memory map of used and unused memory
        AddressRange addressRange = null;
        List<String[]> freeContiguousRangesBlocks;
        if (targetProcess == null || targetProcess.isIsRunning()
                || targetProcess.getProcessID() <= 0) {
            throw new IllegalArgumentException("Please specify a valid process.");
        } else {
            freeContiguousRangesBlocks = listFreeContiguousRanges(targetProcess.getMemoryHeapRequest());
            if (freeContiguousRangesBlocks == null || freeContiguousRangesBlocks.isEmpty()) { //means no more memory to allocate
                throw new OutOfMemoryException("Memory is full. Please close some process to create space");
            }
        }


        switch (selectedAlgorithm) {
            case FIRST_FIT: {
                //iterate through random map and find the first memory block that satisfy request and assign.
                AddressRange firstFitBlock[] = null;
                for (String[] rangeBlock : freeContiguousRangesBlocks) {
                    AddressRange[] candidateBlock = getRangesFromFreeMemoryMap(rangeBlock);
                    int sizeOfBlock = calculateSizeOfBlocks(candidateBlock);

                    if (sizeOfBlock - targetProcess.getMemoryHeapRequest() >= 0) {
                        firstFitBlock = candidateBlock;
                        break;
                    }
                    
                }
                addressRange = mergeFreeAddressRangeBlock(firstFitBlock);
                break;
            }
            case WORST_FIT: {
                //Find largest available and assign the range
                AddressRange worstFitBlock[] = null;
                int difference = -1;
                for (String[] rangeBlock : freeContiguousRangesBlocks) {
                    AddressRange[] candidateBlock = getRangesFromFreeMemoryMap(rangeBlock);
                    int sizeOfBlock = calculateSizeOfBlocks(candidateBlock);
                    difference = sizeOfBlock - targetProcess.getMemoryHeapRequest();

                    if (worstFitBlock == null && difference >= 0) {
                        worstFitBlock = candidateBlock;
                    } else if (difference >= 0 && difference >= calculateSizeOfBlocks(worstFitBlock)) {
                        worstFitBlock = candidateBlock;
                    }
                }
                addressRange = mergeFreeAddressRangeBlock(worstFitBlock);
                break;
            }
            default: { //default is BEST_FIT
                //Find range that leaves the smallest space wastage
                int difference = -1;
                AddressRange bestFitBlock[] = null;
                for (String[] rangeBlock : freeContiguousRangesBlocks) {
                    AddressRange[] candidateBlock = getRangesFromFreeMemoryMap(rangeBlock);
                    int sizeOfBlock = calculateSizeOfBlocks(candidateBlock);
                    difference = sizeOfBlock - targetProcess.getMemoryHeapRequest();

                    if (bestFitBlock == null && difference >= 0) {
                        bestFitBlock = candidateBlock;
                    } else if (difference >= 0 && difference < calculateSizeOfBlocks(candidateBlock)) {
                        bestFitBlock = candidateBlock;
                    }
                }


                addressRange = mergeFreeAddressRangeBlock(bestFitBlock); //select the chosen best.
                break;
            }
        }
//        if an addressRange is successfuly chosen, we remove the chosen from free memory map and add in to assigned memmory
        if (addressRange != null) {
            //now lock memory cells to only belong to target process for the program in
            assignedMemoryMap.put(targetProcess.getProcessID(), addressRange);
            //write memory to lock for the assigned process
            for (int i = addressRange.getAddressFrom(); i < (addressRange.getAddressTo() + 1); i++) {
                getMemory().getMemoryBlocks()[i].setOwiningProcess(targetProcess.getProcessID());
            }
            freeMemoryMap.remove(getFreeMemoryMapKeyForRange(addressRange));
        }

        return addressRange;
    }

    /**
     * Merges a given range of address ranges into one.
     *
     * If method succeeds the given blocks will be removed.
     *
     * @param block must be a set of contiguous block.
     * @return the merged block.
     * @throws IllegalOperationException
     */
    private synchronized AddressRange mergeFreeAddressRangeBlock(AddressRange block[]) throws IllegalOperationException {
        AddressRange mergedAddressRange = null;
        if (block == null || block.length == 0) {
            throw new IllegalArgumentException("Cannot merge given address ranges. Null address range");
        } else {
            AddressRange previousRange = null;
            for (AddressRange addressRange : block) {
                if (addressRange == null || !(addressRangesContigous(previousRange, addressRange) || previousRange == null)) {
                    throw new IllegalOperationException("Memory block contains bad sectors.");
                }
                previousRange = addressRange;
            }

            //means all was well.
            //we now sort the blobks by starting address and create one continous block.
            Arrays.sort(block, new MemoryMapAddressComparator());
            mergedAddressRange = new AddressRange(block[0].getAddressFrom(), block[block.length - 1].getAddressTo());
            //now remove each from free memory map.
            for (AddressRange addressRange : block) {
                freeMemoryMap.remove(getFreeMemoryMapKeyForRange(addressRange));
            }
            //add the merged one.
            freeMemoryMap.put(getFreeMemoryMapKeyForRange(mergedAddressRange), mergedAddressRange);
        }
        return mergedAddressRange;
    }

    /**
     * The key for the map to free memory map is generated by multiplying the
     * start address by negative one
     *
     * @param targetAddressRange address range to try retrieve it from free
     * memory map.
     * @return key for accessing the free memory map in the set. a non-existent
     * key is generated if it null process is passed
     */
    public int getFreeMemoryMapKeyForRange(AddressRange targetAddressRange) {
        int key = new Random().nextInt(); //use a non-existent key as default if target is not found
        if (targetAddressRange != null) {
            key = (targetAddressRange.getAddressFrom() * -1);
        }
        return key;
    }

    /**
     * Simply locates memory maps referenced by the process ID and dereferences
     * them and declares them free
     *
     * @param processID the unique identifier of the process
     */
    public synchronized void removeFromMemory(int processID) {
        if (assignedMemoryMap != null && !assignedMemoryMap.isEmpty() //if there are processes
                && assignedMemoryMap.containsKey(processID)) { //and if process exists in the process map
            //take the memory address and clear the cells
            AddressRange addressRangeToFree = assignedMemoryMap.get(processID);
            MemoryBlock[] memoryBlocks = memory.getMemoryBlocks();
            for (int i = addressRangeToFree.getAddressFrom(); i < addressRangeToFree.getAddressTo() + 1; i++) {
                memoryBlocks[i].setIsUsed(false); //whether it has program data or not.
                memoryBlocks[i].setOwiningProcess(0); //set free the process to 0
            }
            //remove the address range from the assigned memory map
            AddressRange rangeToRemove = assignedMemoryMap.get(processID);
            freeMemoryMap.put(getFreeMemoryMapKeyForRange(rangeToRemove), rangeToRemove);
            assignedMemoryMap.remove(processID);
            //add new space to free memoryMap
	    System.out.println("Killed process at " + rangeToRemove);
        }
    }

    /**
     * Rebuilds the free memory map by partitioning memory into sectors.
     *
     * Memory is initially partitioned into max of 16MB sectors. Processes
     * requesting more than this amount will take two blocks of this for
     * instance.
     *
     * The free memory block is assigned a negative value of the start address
     * as the key in the map.
     *
     * It should be called only once at startup for best simulation of how
     * programs get started and killed and leave memory fragmented.
     */
    public final synchronized void formatMemory() {
        //first clear the free memory map.
        freeMemoryMap.clear();
        //then rescan the memory cells and create the address ranges of free memory
        MemoryBlock[] memoryBlocks = memory.getMemoryBlocks();
        boolean isStarted = false;
        int startAddress = 0;
        int blockIndex = 0;
        int blockSize = 0;
        if (memoryBlocks != null) {
            System.out.println("Available memory blocks is: " + memoryBlocks.length);
            for (MemoryBlock memoryBlock : memoryBlocks) {
                if (memoryBlock != null) {
                    if (!isStarted && memoryBlock.getOwningProcess() <= 0) {
                        isStarted = true; //means block is not owned by anybpdy, hence free. we start counting free locations
                        startAddress = memoryBlock.getAddressLocation(); //sets the start cell
                        blockSize = 1; //1mb size to start
                    }
                    if (memoryBlock.getOwningProcess() > 0 && isStarted) {
                        //means this block is in use. so, close the range if count has started and has more than one block
                        //if the difference of the address range is 1; then it is only one cell free. else; the rest will
                        //have range equal to {startCellAddress ~ lastCellAddress}
                        freeMemoryMap.put(startAddress * -1, new AddressRange(startAddress, memoryBlock.getAddressLocation()));
                        isStarted = false;//stop count
                        System.out.println("##FRAGMENTED_BLOCK Created a memory block of: " + blockSize);
                    } else if (blockIndex == (memoryBlocks.length - 1)) { //test if its the last block also. it qualifies to be a memory map is the whole memory is free
                        //close the range
                        freeMemoryMap.put(startAddress * -1, new AddressRange(startAddress, memoryBlock.getAddressLocation()));
                        isStarted = false;//stop count
                        System.out.println("##END_MEMORY_CAPACITY Created a memory block of: " + blockSize);
                    } else if (blockSize == Memory.SECTOR_SIZE) { //if it is sector full
                        freeMemoryMap.put(startAddress * -1, new AddressRange(startAddress, memoryBlock.getAddressLocation()));
                        isStarted = false;//stop count
                        System.out.println("##SECTOR_SIZE Created a memory block of: " + blockSize);
                    }
                }
                blockIndex++;
                blockSize++;
            }
        }
        System.out.println("Free memory map contains blocks: " + freeMemoryMap.size());
    }

    public static Algorithm getSelectedAlgorithm() {
        return selectedAlgorithm;
    }

    public static void setSelectedAlgorithm(Algorithm selectedAlgorithm) {
        MemoryManager.selectedAlgorithm = selectedAlgorithm;
    }

    public static MemoryManager getInstance() {
        return MemoryManagerHolder.MEMORY_MANAGER_INSTANCE;
    }

    private static class MemoryManagerHolder {

        private final static MemoryManager MEMORY_MANAGER_INSTANCE = new MemoryManager();
    }

    /**
     * Calculates used memory by summing sizes of the assigned memory map
     *
     * @return size
     */
    public final int getFreeMemorySize() {
        int size = 0;
        for (Map.Entry<Integer, AddressRange> freeRange : freeMemoryMap.entrySet()) {
            AddressRange addressRange = freeRange.getValue();
            if (addressRange != null) {
                size += addressRange.calculateSize();
            }
        }
        return size;
    }

    /**
     * Calculates available memory by summing sizes of the free memory map
     *
     * @return size
     */
    public final int getUsedMemorySize() {
        int size = 0;
        for (Map.Entry<Integer, AddressRange> assignedRange : assignedMemoryMap.entrySet()) {
            AddressRange addressRange = assignedRange.getValue();
            if (addressRange != null) {
                size += addressRange.calculateSize();
            }
        }
        return size;
    }
}
