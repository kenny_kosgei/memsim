package com.simulator.managers;

import com.simulator.IllegalOperationException;
import com.simulator.OutOfMemoryException;
import com.simulator.Process;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Manages processes by creating, assigning process IDs, terminates/Destroying
 * processes
 *
 * @author Catherine S.
 */
public class ProcessManager {

    private List<com.simulator.Process> processes;
    private Random randomGenerator;
    private Process osProcess; //default process that represents the operating system. //request 140MB ram

    private ProcessManager() {
	randomGenerator = new Random();
	processes = new ArrayList<Process>();
	try {
	    //Request OS process to be created and loaded into memory
	    osProcess = createProcess("Operating System", -1, 140);
	    osProcess.setImageColor(Color.magenta);
	} catch (IllegalArgumentException ex) {
	    System.out.println("Bad arguments sent to Memory Manager. Unable to load defult process: " + ex.getMessage());
	} catch (OutOfMemoryException ex) {
	    System.out.println("Insufficient memory to start memory simulator" + ex.getMessage());
	} catch (IllegalOperationException ex) {
	    System.out.println("Unable to communicate with Memory Manager: " + ex.getMessage());
	}
    }

    /**
     * Generates a random process ID between 0 and 1001
     *
     * @return integer value
     */
    public final int generateProcessID() {
	return randomGenerator.nextInt(1000) + 1;
    }

    public List<Process> getProcesses() {
	return processes;
    }

    /**
     * removes a process from process memory and removes it from the process table.
     * 
     * @param processID the unique process ID of the process
     */
    public synchronized void killProcess(int processID) {
	for (Process process : processes) {
	    if (process != null && process.getProcessID() == processID) {
		MemoryManager.getInstance().removeFromMemory(processID);
		processes.remove(process);
		break;
	    }
	}
    }

    public final synchronized Process createProcess(String procName, long lifeTime, int memorySizeRequest) throws IllegalArgumentException, OutOfMemoryException, IllegalOperationException {
	Process newProcess;
	int processID = generateProcessID();
	if (memorySizeRequest <= 0) {
	    throw new IllegalArgumentException("Memory heap request should be more that zero");
	} else if (lifeTime == 0) {
	    throw new IllegalArgumentException("Lifetime of process should be at least 1 second");
	} else if (procName == null || procName.trim().isEmpty()) {
	    //generate name
	    procName = "process_" + processID;
	}

	//create program instance and start it.

	newProcess = new Process(memorySizeRequest, lifeTime, procName);
	newProcess.setProcessID(processID);
	MemoryManager.getInstance().assignMemory(newProcess);
	processes.add(newProcess);
	System.out.println("Process created: \t" + newProcess.getProcessName() + ": " + MemoryManager.getInstance().getAssignedMemoryMap().get(newProcess.getProcessID()));
	System.out.println("Used Memory: " + MemoryManager.getInstance().getUsedMemorySize());
	System.out.println("Free Memory: " + MemoryManager.getInstance().getFreeMemorySize());
	return newProcess;
    }

    public static ProcessManager getpProcessManager() {
	return ProcessManagerHolder.PROCESS_MANAGER_INSTANCE;
    }

    private static class ProcessManagerHolder {

	private final static ProcessManager PROCESS_MANAGER_INSTANCE = new ProcessManager();
    }
}
