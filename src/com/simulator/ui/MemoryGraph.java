package com.simulator.ui;

import com.simulator.AddressRange;
import com.simulator.managers.MemoryManager;
import com.simulator.managers.ProcessManager;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;

/**
 * This class draws out the memory partitions and usages.
 *
 * @author Catherine S.
 */
public class MemoryGraph extends JPanel {

    private List<com.simulator.Process> processes;
    private MemoryManager memoryManager;
    private boolean showFreePartitions = false;

    public MemoryGraph() {
	memoryManager = MemoryManager.getInstance();
	addMouseListener(new MouseAdapter() {
	    @Override
	    public void mouseClicked(MouseEvent e) {
		updateGraph(ProcessManager.getpProcessManager().getProcesses());
	    }
	});
    }

    public synchronized void updateGraph(List<com.simulator.Process> processes) {
	this.processes = processes;
	repaint();
    }

    public boolean isShowFreePartitions() {
	return showFreePartitions;
    }

    public void setShowFreePartitions(boolean showFreePartitions) {
	this.showFreePartitions = showFreePartitions;
	//refresh
	updateGraph(processes);
    }

    @Override
    public void paint(Graphics g) {
	super.paint(g);
	this.setBackground(Color.WHITE);
	g.setColor(Color.cyan);
	g.drawRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
	int calculatedWidth;//scaled width
	int calculatedStartPosition = 0; //the equivalent X position
	final double scale = this.getWidth() / 2048.0; //The ratio of width of the canvas vs memory size 

	//draw partitoins of free memory first
	if (showFreePartitions) {
	    Map<Integer, AddressRange> freeMemoryMap = memoryManager.getFreeMemoryMap();
	    if (freeMemoryMap != null && !freeMemoryMap.isEmpty()) {
		for (Map.Entry<Integer, AddressRange> freeBlockkey : freeMemoryMap.entrySet()) {
		    AddressRange freeBlock = freeBlockkey.getValue();
		    if (freeBlock != null) {
			//draw out block
			calculatedWidth = (int) freeBlock.calculateSize();
			if (calculatedWidth < 1) {
			    //ceil the value to one
			    calculatedWidth = (int) Math.ceil(freeBlock.calculateSize());
			}

			if (freeBlock != null) {
			    calculatedStartPosition = (int) (freeBlock.getAddressFrom() * scale);
			}
			g.drawRect(calculatedStartPosition, this.getY(), calculatedWidth, this.getHeight());
		    }
		}
	    }
	}
	
	//draw mem locations against this rect
	if (processes != null && !processes.isEmpty()) {
	    AddressRange addressRange;
	    for (com.simulator.Process process : processes) {
		if (process != null && process.getMemoryHeapRequest() > 0) {
		    calculatedWidth = (int) (process.getMemoryHeapRequest() * scale);
		    if (calculatedWidth < 1) {
			//ceil the value to one
			calculatedWidth = 1;
		    }
		    addressRange = memoryManager.getAssignedMemoryMap().get(process.getProcessID());
		    if (addressRange != null) {
			calculatedStartPosition = (int) (addressRange.getAddressFrom() * scale);
		    }
		    if (process.getImageColor() != null) {
			g.setColor(process.getImageColor());
		    }else{
			g.setColor(Color.gray);
		    }
		    g.fillRect(calculatedStartPosition, this.getY(), calculatedWidth, this.getHeight());
		}
	    }
	}

    }
}
