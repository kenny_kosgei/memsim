/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simulator.ui.helpers;

import com.simulator.managers.MemoryManager;
import java.awt.Color;

/**
 *
 * @author Catherine S.
 */
public class ProcessTableModel extends javax.swing.table.AbstractTableModel {

    private Object[][] matrix;
    private final String columnHeaders[] = {"Process ID", "Name", "Requested Memory", "Assigned Heap", ""};

    public ProcessTableModel(com.simulator.Process[] processes) {
	if (processes == null || processes.length == 0) {
	    //create an empty table
	     matrix = new Object[columnHeaders.length][0];
	} else {
	    matrix = new Object[processes.length][columnHeaders.length];
	    System.out.println("Creating new process table with " + processes.length + " processes");

	    for (int j = 0; j < processes.length; j++) {
		for (int i = 0; i < columnHeaders.length; i++) {
		    Object item = null;
		    switch (i){
			case 0:{
			    item = processes[j].getProcessID();
			    break;
			}
			case 1:{
			    item = processes[j];
			    break;
			}
			case 2:{
			    item = processes[j].getMemoryHeapRequest();
			    break;
			}
			
			case 3:{
			    item = MemoryManager.getInstance().getAssignedMemoryMap().get(processes[j].getProcessID());
			    break;
			}
			
			case 4:{ //process color
			    item = processes[j].getImageColor();
			    break;
			}
		    }
		    matrix[j][i] = item;
		}
	    }
	}

    }

    @Override
    public String getColumnName(int column) {
	return columnHeaders[column];
    }
    

    @Override
    public int getRowCount() {
	return matrix.length;
    }

    @Override
    public int getColumnCount() {
	return columnHeaders.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
	try {
	    return matrix[rowIndex][columnIndex];
	} catch (Exception e) {
	    return null;
	}
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
	if (columnIndex == 4) {
	    return Color.class;
	}
	return Object.class;
    }
}
