package com.simulator.util;

import com.simulator.AddressRange;
import java.util.Comparator;

/**
 * Compares two Address Ranges and sorts them by linear location in memory.
 *
 * @author Catherine S.
 */
public class MemoryMapAddressComparator implements Comparator<AddressRange> {

    @Override
    public int compare(AddressRange rangeOne, AddressRange rangeTwo) {
	return rangeOne.getAddressFrom() - rangeTwo.getAddressFrom();
    }

}
