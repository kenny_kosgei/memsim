package com.simulator.util;

import com.simulator.AddressRange;
import java.util.Comparator;

/**
 * Compares two Address Ranges and sorts them by Size of block.
 *
 * @author Catherine S.
 */
public class MemoryMapSizeComparator implements Comparator<AddressRange> {

    @Override
    public int compare(AddressRange rangeOne, AddressRange rangeTwo) {
	return rangeOne.calculateSize() - rangeTwo.calculateSize();
    }

}
