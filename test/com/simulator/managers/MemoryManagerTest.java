package com.simulator.managers;

import com.simulator.AddressRange;
import com.simulator.Algorithm;
import com.simulator.Memory;
import com.simulator.Process;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Catherine S.
 */
public class MemoryManagerTest {
    
    MemoryManager memoryManager;
    
    public MemoryManagerTest() {
    }
    
    @Before
    public void setUp() {
        memoryManager = MemoryManager.getInstance();
        memoryManager.formatMemory();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMemory method, of class MemoryManager.
     */
    @Test
    public void testGetMemory() {
        System.out.println("getMemory");
        assertNotNull("Memory should not ne null", memoryManager.getMemory());
    }

    /**
     * Test of getFreeMemoryMap method, of class MemoryManager.
     */
    @Test@Ignore
    public void testGetFreeMemoryMap() {
        System.out.println("getFreeMemoryMap");
        memoryManager.formatMemory();
        Map<Integer, AddressRange> freeMemoryMap = memoryManager.getFreeMemoryMap();
        assertTrue(freeMemoryMap != null);
    }

    /**
     * Test of assignMemory method, of class MemoryManager.
     */
    @Test
    public void testAssignMemory() throws Exception {
        System.out.println("assignMemory");
        Process targetProcess = new Process(36, 60000, "Test Process_1");
        targetProcess.setProcessID(53);
        AddressRange assignedMemory = memoryManager.assignMemory(targetProcess);
        System.out.println("Assigned Memory is: " + assignedMemory + "\n&&Before Updating free memory");
        System.out.println("&&free memory contains: " + memoryManager.getFreeMemoryMap().size());
        System.out.println("&&Assigned memory contains: " + memoryManager.getAssignedMemoryMap().size());
        targetProcess = new Process(12, 40000, "Test Process_2");
        targetProcess.setProcessID(65);
        memoryManager.assignMemory(targetProcess);
        System.out.println("&&used memory: " + memoryManager.getUsedMemorySize() );
        System.out.println("&&free memory: " + memoryManager.getFreeMemorySize());
        System.out.println("$$Used memory map: " + memoryManager.getAssignedMemoryMap());
        System.out.println("Reading out free memory blocks:\n\n");
        for (Map.Entry<Integer, AddressRange> free : memoryManager.getFreeMemoryMap().entrySet()) {
            AddressRange addressRange = free.getValue();
            System.out.println("@" + addressRange);
        }
    }

    /**
     * Test of removeFromMemory method, of class MemoryManager.
     */
    @Test@Ignore
    public void testRemoveFromMemory() {
        System.out.println("removeFromMemory");
        int processID = 0;
        MemoryManager instance = null;
        instance.removeFromMemory(processID);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updateFreeMemory method, of class MemoryManager.
     */
    @Test@Ignore
    public void testFormatMemory() {
        memoryManager.formatMemory();
        Map<Integer, AddressRange> freeMemoryMap = memoryManager.getFreeMemoryMap();
//        assertTrue(freeMemoryMap != null);
    }

    /**
     * Test of getSelectedAlgorithm method, of class MemoryManager.
     */
    @Test@Ignore
    public void testGetSelectedAlgorithm() {
        System.out.println("getSelectedAlgorithm");
        Algorithm expResult = null;
        Algorithm result = MemoryManager.getSelectedAlgorithm();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedAlgorithm method, of class MemoryManager.
     */
    @Test@Ignore
    public void testSetSelectedAlgorithm() {
        System.out.println("setSelectedAlgorithm");
        Algorithm selectedAlgorithm = null;
        MemoryManager.setSelectedAlgorithm(selectedAlgorithm);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInstance method, of class MemoryManager.
     */
    @Test@Ignore
    public void testGetInstance() {
        System.out.println("getInstance");
        MemoryManager expResult = null;
        MemoryManager result = MemoryManager.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMemory method, of class MemoryManager.
     */
    @Test@Ignore
    public void testSetMemory() {
        System.out.println("setMemory");
        Memory memory = null;
        MemoryManager instance = null;
        instance.setMemory(memory);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAssignedMemoryMap method, of class MemoryManager.
     */
    @Test@Ignore
    public void testGetAssignedMemoryMap() {
        System.out.println("getAssignedMemoryMap");
        MemoryManager instance = null;
        Map expResult = null;
        Map result = instance.getAssignedMemoryMap();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFreeMemoryMapKeyForRange method, of class MemoryManager.
     */
    @Test@Ignore
    public void testGetFreeMemoryMapKeyForRange() {
        System.out.println("getFreeMemoryMapKeyForRange");
        AddressRange targetAddressRange = null;
        MemoryManager instance = null;
        int expResult = 0;
        int result = instance.getFreeMemoryMapKeyForRange(targetAddressRange);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFreeMemorySize method, of class MemoryManager.
     */
    @Test
    public void testGetFreeMemorySize() {
        System.out.println("getFreeMemorySize: " + memoryManager.getFreeMemorySize());
        
    }

    /**
     * Test of getUsedMemorySize method, of class MemoryManager.
     */
    @Test
    public void testGetUsedMemorySize() {
        System.out.println("getUsedMemorySize: " + memoryManager.getAssignedMemoryMap());
        
    }
}