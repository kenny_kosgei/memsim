/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simulator.managers;

import com.simulator.OutOfMemoryException;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kenny
 */
public class ProcessManagerTest {

    ProcessManager processManager;

    public ProcessManagerTest() {
    }

    @Before
    public void setUp() {
        processManager = ProcessManager.getpProcessManager();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of generateProcessID method, of class ProcessManager.
     */
    @Test
    public void testGenerateProcessID() {
        System.out.println("generateProcessID");
        System.out.println("%pid: " + processManager.generateProcessID());
    }

    /**
     * Test of getProcesses method, of class ProcessManager.
     */
    @Test
    public void testGetProcesses() {
        System.out.println("getProcesses");

    }

    /**
     * Test of killProcess method, of class ProcessManager.
     */
    @Test
    public void testKillProcess() {
        System.out.println("killProcess");

    }

    /**
     * Test of createProcess method, of class ProcessManager.
     */
    @Test
    public void testCreateProcess() throws Exception {
        System.out.println("createProcess");

        try {
            processManager.createProcess("mimi", 65, 0); //invalid heap request
            fail("invalid heap request accepted");
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }
        try {
            processManager.createProcess("mimi", 0, 85); //invalid lifetime
            fail("invalid lifetime accepted");
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
        }
        try {
            processManager.createProcess("mimi", 23000, 2049); //invalid memory size request
            fail("invalid memory request accepted");
        } catch (Exception e) {
            assertTrue(e instanceof OutOfMemoryException);
        }
        assertNotNull(processManager.createProcess("taskmanager", 2000, 17));

        assertNotNull(processManager.createProcess(null, 120000, 12));
    }

    /**
     * Test of getpProcessManager method, of class ProcessManager.
     */
    @Test
    public void testGetpProcessManager() {
        System.out.println("getpProcessManager");

    }
}